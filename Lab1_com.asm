.model tiny
.code 
start:
    org 0x100
    mov ah, 0x9
    mov dx, offset msg1
    int 0x21
    
    mov ah, 0x9
    mov dx, offset msg2
    int 0x21
    ret
    
    msg1 db "Hello, world!", 10, 13, '$'
    msg2 db "Hello, assembly!", 10, 13, '$'
end start