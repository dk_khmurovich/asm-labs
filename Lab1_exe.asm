.model small
.stack 0x100
.data
    msg1 db "Hello, world!", 10, 13, '$'
    msg2 db "Hello, assembly!", 10, 13, '$'
.code
start:
    mov ax, @data
    mov ds, ax
    
    mov dx, offset msg1
    mov ah, 0x09
    int 0x21
    
    mov dx, offset msg2
    mov ah, 0x09
    int 0x21
    
end start